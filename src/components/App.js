import React from "react";
import {Navbar, Nav, NavDropdown, Form, Button, FormControl, Carousel, CarouselItem} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from './Home';
import Libraries from './Libraries';
import Museums from './Museums';
import Events from './Events';
import About from './About'
import '../styles/App.css';

function App() {
  return (
    <Router>
      <div>
          <BnBNavbar />
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/libraries" component={Libraries} />
          <Route path="/museums" component={Museums} />
          <Route path="/events" component={Events} />
          <footer class="py-5 bg-dark">
            <div class="container">
              <p class="m-0 text-center text-white">CS373: Software Engineering, 11 AM, Group 5</p>
            </div>
          </footer>
      </div>
    </Router>
  );
}

function BnBNavbar() {
  return (
    <Nav className="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div className="container">
        <a href="index.html"><img src="images/logo.png" className="left"></img></a>
        <a className="navbar-brand" href="/">Books and Bones</a>
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link" href="/about">About</a>
            </li>
            <li className="nav-item">
              <Nav.Link href="/libraries">Libraries</Nav.Link>
            </li>
              <li className="nav-item">
              <Nav.Link href="/museums">Museums</Nav.Link>
            </li>
            <li className="nav-item">
              <Nav.Link href="/events">Events</Nav.Link>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/map">Map</a>
            </li>
          </ul>
        </div>
      </div>
    </Nav>
  );
}

export default App;