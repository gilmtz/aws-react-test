import React from "react";
import {Navbar, Nav, NavDropdown, Form, Button, FormControl, Carousel, CarouselItem} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


function Home() {
    return   (
        <div>
        <header>
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner" role="listbox">
                
                <Carousel.Item className="active">
                <img className="d-block w-100" src="images/dinosaur.jpg" alt="first slide"></img>
                </Carousel.Item>
                
                <Carousel.Item>
                <img className="d-block w-100" src="images/library.jpg" alt="second slide"></img>
                </Carousel.Item>
                
                <Carousel.Item>
                <img className="d-block w-100" src="images/aestheticbooks.jpg" alt="third slide"></img>
                </Carousel.Item>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
            </div>
        </header>
        <div className="container">
            <HomePageContent/>
        </div>
        </div>
    );
    }
function HomePageContent(){
    return (
        <div className="container">
            <h1 className="my-4">Educational Places and Events in Austin</h1>
            <div className="row">
                <ModelPortals header = "Places">
                    <p className="card-text">Checkout <a href="libraries.html"> libraries</a> and <a href="museums.html">museums</a> near you</p>
                </ModelPortals>
                <ModelPortals header = "Events">
                    <p className="card-text">Checkout a calendar and <a href="events.html">events</a> near you</p>
                </ModelPortals>
                <ModelPortals header = "Map">
                    <p className="card-text">Checkout a calendar and <a href="events.html">events</a> near you</p>
                </ModelPortals>
                <Portfolio/>
            </div>

        </div>
    );
    }

function ModelPortals(props){
    return (
        <div className="col-lg-4 mb-4">
            <div className="card h-100">
                <h4 className="card-header">{props.header}</h4>
                <div className="card-body">
                    {props.children}
                </div>
            </div>
        </div>
    );
    }

function Portfolio(props){
    return(
        <div>
        <h2>Check out some here!</h2>
        <EventCard 
            img = "time_span.jpg" 
            title = "Annual Celebration of Nancy Holt's Time Span"
            description = "Come see how, for one moment each year, the steel wheel of Nancy Holt's 1981 work Time Span casts a shadow around a plaque in the earth inscribed with the day’s date."
            website = "https://www.thecontemporaryaustin.org/event/annual-celebration-of-nancy-holts-time-span-3/"
            datetime = "Friday, April 5, 2019: 3:00 pm - Friday, April 5, 2019: 7:00 pm"
        />
        <PlaceCard
            img = "laguna-glora.jpg"
            title = "Laguna Gloria Art Museum"
            description = "he Contemporary Austin - Laguna Gloria, formerly known as the AMOA-Arthouse at Laguna Gloria, is the former home of Clara Driscoll and site of a 1916 Italianate-style villa on the shores of Lake Austin in Austin, Texas(...)"
            address = "3809 West 35th St, Austin, TX, 78703"
            website = "https://www.thecontemporaryaustin.org/"
        />
        </div>
    );
    }
function EventCard(props){
    return (
        <div className="col-lg-4 col-sm-6 portfolio-item">
        <div className="card h-100">
            <a href="#"><img className="card-img-top" src={props.img} alt=""/></a>
            <div className="card-body">
            <h4 className="card-title">
                <a href="event-instance-1.html">{props.title}</a>
            </h4>
            <div className="card-body">
            <p className="card-text">{props.description}</p>
            <p className="card-text"><a href={props.website}/>Website</p>
            </div>
            <div className="card-footer">
            <a href="event-instance-1.html" className="btn btn-primary">{props.datetime}</a>
            </div>
            </div>
        </div>
        </div>

    );
    }
function PlaceCard(props){
    return (
        <div className="col-lg-4 col-sm-6 portfolio-item">
        <div className="card h-100">
            <a href="#"/><img className="card-img-top" src={props.img} alt=""/>
            <div className="card-body">
            <h4 className="card-title">
                <a href="museum-instance-1.html">{props.title}</a>
            </h4>
            <p className="card-text">{props.description}</p>
            <p className="card-text">{props.address}</p>
            <p className="card-text"><a href={props.website}/>Website</p>
            </div>
        </div>
        </div>

    );
    }
export default Home;